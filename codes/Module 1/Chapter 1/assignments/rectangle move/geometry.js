var geometry;
(function (geometry) {
    var Point = /** @class */ (function () {
        function Point(x, y) {
            this.x = x;
            this.y = y;
        }
        return Point;
    }());
    geometry.Point = Point;
    var rectangle = /** @class */ (function () {
        function rectangle(context, l, w, stpt) {
            this._l = l;
            this._w = w;
            this._stpt = stpt;
            this.context = context;
            this.s = 1;
            this._stxcoord = this._stpt.x;
            this._stycord = this._stpt.y;
            if (typeof this.color != 'undefined') {
                this.color = this.color;
            }
            else {
                this.color = "red";
            }
        }
        rectangle.prototype.draw = function () {
            this.context.beginPath();
            this.context.lineWidth = 4;
            this.context.rect(this._stpt.x, this._stpt.y, this._l, this._w);
            this.context.strokeStyle = "black";
            this.context.fillStyle = this.color;
            this.context.fill();
            this.context.stroke();
        };
        rectangle.prototype.tofromotion = function (tofrolength) {
            this._stpt.x += this.s;
            if (this._stxcoord + tofrolength <= this._stpt.x) {
                this.s = -1;
            }
            else if (this._stxcoord - tofrolength > this._stpt.x) {
                this.s = 1;
            }
        };
        return rectangle;
    }());
    geometry.rectangle = rectangle;
})(geometry || (geometry = {}));
