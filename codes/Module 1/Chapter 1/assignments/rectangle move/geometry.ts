namespace geometry {
    export class Point {
        public x : number;
        public y : number;
        constructor(x:number,y:number) {
            this.x=x;
            this.y=y;
        }
    }

    export class rectangle 
    {
        private _l:number;
        private _w:number;
        private _stpt:Point;
        private _stxcoord: number;
        private _stycord : number;
        private _endxcord : number;
        private s: number;
        public color:string;
        public context: CanvasRenderingContext2D;
        private tofrolength : number;
        private tofrolength1 : number;

        constructor(context:CanvasRenderingContext2D, l:number, w:number, stpt:Point) 
        {
            this._l=l;
            this._w=w;
            this._stpt=stpt;
            this.context=context;
            this.s=1;
            this._stxcoord=this._stpt.x;
            this._stycord=this._stpt.y;

            if (typeof this.color!= 'undefined') 
            {
                this.color= this.color;
            } else 
            {
                this.color="red";
            }
        }

        draw() 
        {
            this.context.beginPath();
            this.context.lineWidth = 4;
            this.context.rect(this._stpt.x,this._stpt.y,this._l,this._w);
            this.context.strokeStyle = "black";
            this.context.fillStyle= this.color;
            this.context.fill();
            this.context.stroke();
        }

        tofromotion (tofrolength:number)
        {
            this._stpt.x += this.s;
            if(this._stxcoord+tofrolength<=this._stpt.x)
            {
                this.s=-1;
            } 
            else if (this._stxcoord-tofrolength>this._stpt.x) 
            {
                this.s=1;
            }
        }


       //  motion (tofrolength1:number) 
       // {
               // this._stpt.y += this.s;
              //  if(this._stxcoord+tofrolength1<=this._stpt.y) 
              //  {
              //      this.s=-1;
              //  }
              //   else if (this._stxcoord-tofrolength1>this._stpt.y) 
               // {
                   // this.s=1;
               // }
        //}
    }
}