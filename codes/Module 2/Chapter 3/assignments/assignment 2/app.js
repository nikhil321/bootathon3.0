function check() {
    var x = document.getElementById("t1");
    var y = document.getElementById("t2");
    var z = document.getElementById("t3");
    var a = +x.value;
    var b = +y.value;
    var c = +z.value;
    if (a == b && b == c) {
        document.getElementById("b1").innerHTML = "The Triangle Is Equilateral";
    }
    else if ((a != b || b != c) && (a == b || b == c || c == a)) {
        if (((a * a) == (b * b) + (c * c)) || ((b * b) == (c * c) + (a * a)) || ((c * c) == (a * a) + (b * b))) {
            document.getElementById("b2").innerHTML = "It is a right angled triangle.";
        }
        document.getElementById("b1").innerHTML = "It is an isosceles triangle.";
    }
    else if (a != b && b != c) {
        if (((a * a) == (b * b) + (c * c)) || ((b * b) == (c * c) + (a * a)) || ((c * c) == (a * a) + (b * b))) {
            document.getElementById("b2").innerHTML = "It is a right angled triangle.";
        }
        document.getElementById("b1").innerHTML = "It is a scalene triangle.";
    }
}
//# sourceMappingURL=app.js.map