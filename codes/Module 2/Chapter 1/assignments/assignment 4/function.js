function cal() {
    var a = document.getElementById("a");
    var r = parseFloat(a.value);
    var area = Math.PI * Math.pow(r, 2);
    document.getElementById("area").innerHTML = "<b>Area of given radius is: </b>" + area;
}
function calci() {
    var a = document.getElementById("a");
    var val = +a.value;
    if (val == 1) {
        add();
    }
    else if (val == 2) {
        sub();
    }
    else if (val == 3) {
        mult();
    }
    else if (val == 4) {
        div();
    }
    else if (val == 5) {
        squareroot();
    }
    else if (val == 6) {
        power();
    }
    else if (val == 7) {
        sin();
    }
    else if (val == 8) {
        cos();
    }
    else if (val == 9) {
        tan();
    }
    else {
        alert("Enter a valid choice");
    }
}
function add() {
    var n = +prompt("Enter the no. of values u want to add");
    var sum = 0;
    for (var i = 1; i <= n; i++) {
        var a = +prompt("Enter the numbers");
        sum += a;
    }
    document.getElementById("result").innerHTML = "Sum of all the numbers are " + sum;
}
function sub() {
    var m = +prompt("Enter the 1st no. from which u want to substract");
    var n = +prompt("Enter the no. to be substracted");
    var dif = m - n;
    document.getElementById("result").innerHTML = "<b>Difference of all the numbers are </b>" + dif;
}
function mult() {
    var n = +prompt("Enter the no. of values u want to multiply");
    var sum = 1;
    for (var i = 1; i <= n; i++) {
        var a = +prompt("Enter the numbers");
        sum = sum * a;
    }
    document.getElementById("result").innerHTML = "<b>Difference of all the numbers are </b>" + sum;
}
function div() {
    var m = +prompt("Enter the dividend value ");
    var n = +prompt("Enter the divisor");
    if (n != 0) {
        var a = m / n;
        document.getElementById("result").innerHTML = "<b> Quotient is </b>" + a;
    }
    else if (n == 0) {
        alert("Divisor cannot be zero");
    }
}
function squareroot() {
    var m = +prompt("Enter the number for which you want to find Square Root.");
    document.getElementById("result").innerHTML = "<b> Square Root is : </b>" + Math.sqrt(m);
}
function power() {
    var m = +prompt("Enter the number you want ");
    var n = +prompt("Enter the power ");
    document.getElementById("result").innerHTML = "<b> Power of the no. is : </b>" + Math.pow(m, n);
}
function sin() {
    var m = +prompt("Enter the angle in degree");
    document.getElementById("result").innerHTML = "<b> Sine of the angle is : </b>" + Math.sin(m * Math.PI / 180);
}
function cos() {
    var m = +prompt("Enter the angle in degree");
    document.getElementById("result").innerHTML = "<b> Cosine of the angle is : </b>" + Math.cos(m * Math.PI / 180);
}
function tan() {
    var m = +prompt("Enter the angle in degree");
    document.getElementById("result").innerHTML = "<b> Tan of the angle is : </b>" + Math.tan(m * Math.PI / 180);
}
function given_result() {
    var a = document.getElementById("s1");
    var x = parseFloat(a.value);
    var req_value = Math.pow(x, 2) - Math.cos(x * Math.PI / 180);
    document.getElementById("result_5").innerHTML = "Required answer is : " + req_value;
}
function triangle_area() {
    var a1 = document.getElementById("x1");
    var a2 = document.getElementById("x2");
    var a3 = document.getElementById("x3");
    var b1 = document.getElementById("y1");
    var b2 = document.getElementById("y2");
    var b3 = document.getElementById("y3");
    var x1 = parseFloat(a1.value);
    var x2 = parseFloat(a2.value);
    var x3 = parseFloat(a3.value);
    var y1 = parseFloat(b1.value);
    var y2 = parseFloat(b2.value);
    var y3 = parseFloat(b3.value);
    var a = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    var b = Math.sqrt(Math.pow(x2 - x3, 2) + Math.pow(y2 - y3, 2));
    var c = Math.sqrt(Math.pow(x3 - x1, 2) + Math.pow(y3 - y1, 2));
    console.log(a, b, c);
    var total_area = (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2;
    if (total_area < 0) {
        var total_area = -1 * total_area;
    }
    document.getElementById("result").innerHTML = "Total area of the triangle is : " + total_area;
}
//# sourceMappingURL=function.js.map