function string()
{
    var a : HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var b : string = "virtual Labs Bootathon 2020";
    var c : string;
    var d : string;
    var array: string[] =[];

    // string for example:typescript

    //task 1:
    //we have to display string in uppercase:

    document.getElementById("d1").innerHTML += "</br><b>the original string is : </b>" + b;

    c = b.toUpperCase();
    document.getElementById("d1").innerHTML += "</br><b>uppercase is :</b>" + c;


    //task 2:
    //we have to display string in lowercase:

    d= b.toLowerCase();
    document.getElementById("d1").innerHTML += "</br><b>lowercase is: </b>" + d;

    //task 3:
    //split the elements of string

    array = b.split(" ");
    document.getElementById("d1").innerHTML += "</br><b>1st word  :</b>" + array[0];
    document.getElementById("d1").innerHTML += "</br><b>2 nd word :</b>" + array[1];

    document.getElementById("d1").innerHTML += "</br><b>3 rd word :</b>" + array[2];



}